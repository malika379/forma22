package id.co.nexsoft.formative22.service;

import id.co.nexsoft.formative22.model.Product;
import id.co.nexsoft.formative22.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProduct(){
        return productRepository.findAll();
    }

    public Product getProductById(int id){
        return productRepository.findById(id);
    }

    public Product insertProduct(Product product){
        return productRepository.save(product);
    }


}
