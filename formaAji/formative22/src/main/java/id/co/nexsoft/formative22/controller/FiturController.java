package id.co.nexsoft.formative22.controller;

import id.co.nexsoft.formative22.model.Fitur;
import id.co.nexsoft.formative22.service.FiturService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FiturController {
    @Autowired
    private FiturService fiturService;

    @GetMapping("/api/fitur")
    public List<Fitur> getAllFitur(){
        return fiturService.getAllFitur();
    }

    @GetMapping("/api/fitur/{id}")
    public Fitur getFiturById(@PathVariable(value = "id") int id){
        return fiturService.getFiturById(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/api/fitur", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String addFitur(@RequestBody Fitur fitur){
        fiturService.insertFitur(fitur);
        return "success";
    }
}
