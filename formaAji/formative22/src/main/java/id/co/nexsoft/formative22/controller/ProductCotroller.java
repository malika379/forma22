package id.co.nexsoft.formative22.controller;

import id.co.nexsoft.formative22.model.Fitur;
import id.co.nexsoft.formative22.model.Product;
import id.co.nexsoft.formative22.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductCotroller {
    @Autowired
    private ProductService productService;

    @GetMapping("/api/product")
    public List<Product> getAllProduct(){
        return productService.getAllProduct();
    }

    @GetMapping("/api/product/{id}")
    public Product getProductById(@PathVariable(value = "id") int id){
        return productService.getProductById(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/api/product", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String addProduct(@RequestBody Product product){
        productService.insertProduct(product);
        return "success";
    }
}
