package id.co.nexsoft.formative22.service;

import id.co.nexsoft.formative22.model.Fitur;
import id.co.nexsoft.formative22.model.History;
import id.co.nexsoft.formative22.repository.HistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryService {
    @Autowired
    private HistoryRepository historyRepository;

    public List<History> getAllHistory(){
        return historyRepository.findAll();
    }

    public History getHistoryById(int id){
        return historyRepository.findById(id);
    }

    public History insertHistory(History history){
        return historyRepository.save(history);
    }
}
