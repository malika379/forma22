package id.co.nexsoft.formative22.service;

import id.co.nexsoft.formative22.model.Testimonial;
import id.co.nexsoft.formative22.repository.TestimonialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestimonialService {
    @Autowired
    private TestimonialRepository testimonialRepository;

    public List<Testimonial> getAllTestimonial(){
        return testimonialRepository.findAll();
    }

    public Testimonial getTestimonialById(int id){
        return testimonialRepository.findById(id);
    }

    public Testimonial insertTestimonial(Testimonial testimonial){
        return testimonialRepository.save(testimonial);
    }
}
