package id.co.nexsoft.formative22.repository;

import id.co.nexsoft.formative22.model.Tab;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TabRepository extends CrudRepository<Tab, Integer> {
    Tab findById(int id);
    List<Tab> findAll();
    void deleteById(int id);
}
