package id.co.nexsoft.formative22.controller;

import id.co.nexsoft.formative22.model.Product;
import id.co.nexsoft.formative22.model.Tab;
import id.co.nexsoft.formative22.service.TabService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TabController {
    @Autowired
    private TabService tabService;

    @GetMapping("/api/tab")
    public List<Tab> getAllTab(){
        return tabService.getAllTab();
    }

    @GetMapping("/api/tab/{id}")
    public Tab getTabById(@PathVariable(value = "id") int id){
        return tabService.getTabById(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/api/tab", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String addTab(@RequestBody Tab tab){
        tabService.insertTab(tab);
        return "success";
    }
}
