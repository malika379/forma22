package id.co.nexsoft.formative22.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Testimonial {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String testi;

    public Testimonial() {
    }

    public Testimonial(int id, String name, String testi) {
        this.id = id;
        this.name = name;
        this.testi = testi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTesti() {
        return testi;
    }

    public void setTesti(String testi) {
        this.testi = testi;
    }
}
