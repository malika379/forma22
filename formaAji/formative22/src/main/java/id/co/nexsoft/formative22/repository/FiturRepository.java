package id.co.nexsoft.formative22.repository;

import id.co.nexsoft.formative22.model.Fitur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FiturRepository extends CrudRepository<Fitur, Integer> {
    Fitur findById(int id);
    List<Fitur> findAll();
    void deleteById(int id);
}
