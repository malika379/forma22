package id.co.nexsoft.formative22.service;

import id.co.nexsoft.formative22.model.Fitur;
import id.co.nexsoft.formative22.repository.FiturRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FiturService {
    @Autowired
    private FiturRepository fiturRepository;

    public List<Fitur> getAllFitur(){
        return fiturRepository.findAll();
    }

    public Fitur getFiturById(int id){
        return fiturRepository.findById(id);
    }

    public Fitur insertFitur(Fitur fitur){
        return fiturRepository.save(fitur);
    }

    public String updateFitur (Fitur fitur, int id){
        Fitur fiturTemp = fiturRepository.findById(id);

        if (fiturTemp == null){
            return "tidak ditemukan";
        }

        fitur.setId(id);
        fiturRepository.save(fiturTemp);
        return "Success Update";
    }
}
