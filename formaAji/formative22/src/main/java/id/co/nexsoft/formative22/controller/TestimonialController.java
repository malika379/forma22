package id.co.nexsoft.formative22.controller;

import id.co.nexsoft.formative22.model.Product;
import id.co.nexsoft.formative22.model.Testimonial;
import id.co.nexsoft.formative22.service.TestimonialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TestimonialController {
    @Autowired
    private TestimonialService testimonialService;

    @GetMapping("/api/testimonial")
    public List<Testimonial> getAllTestimonial(){
        return testimonialService.getAllTestimonial();
    }

    @GetMapping("/api/testimonial/{id}")
    public Testimonial getTestimonialById(@PathVariable(value = "id") int id){
        return testimonialService.getTestimonialById(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/api/testimonial", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String addTestimonial(@RequestBody Testimonial testimonial){
        testimonialService.insertTestimonial(testimonial);
        return "success";
    }
}
