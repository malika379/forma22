package id.co.nexsoft.formative22.controller;

import id.co.nexsoft.formative22.model.History;
import id.co.nexsoft.formative22.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HistoryController {
    @Autowired
    private HistoryService historyService;

    @GetMapping("/api/history")
    public List<History> getAllHistory(){
        return historyService.getAllHistory();
    }

    @GetMapping("/api/history/{id}")
    public History getHistoryById(@PathVariable(value = "id") int id){
        return historyService.getHistoryById(id);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/api/history", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public String addHistory(@RequestBody History history){
        historyService.insertHistory(history);
        return "success";
    }
}
