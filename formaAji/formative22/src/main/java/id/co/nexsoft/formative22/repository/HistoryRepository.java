package id.co.nexsoft.formative22.repository;

import id.co.nexsoft.formative22.model.History;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends CrudRepository<History, Integer> {
    History findById(int id);
    List<History> findAll();
    void deleteById(int id);
}
