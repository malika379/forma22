package id.co.nexsoft.formative22.repository;

import id.co.nexsoft.formative22.model.Testimonial;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestimonialRepository extends CrudRepository<Testimonial, Integer> {
    Testimonial findById(int id);
    List<Testimonial> findAll();
    void deleteById(int id);
}
