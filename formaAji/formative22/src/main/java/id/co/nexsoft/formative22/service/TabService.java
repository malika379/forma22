package id.co.nexsoft.formative22.service;

import id.co.nexsoft.formative22.model.Product;
import id.co.nexsoft.formative22.model.Tab;
import id.co.nexsoft.formative22.repository.TabRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TabService {
    @Autowired
    private TabRepository tabRepository;

    public List<Tab> getAllTab(){
        return tabRepository.findAll();
    }

    public Tab getTabById(int id){
        return tabRepository.findById(id);
    }

    public Tab insertTab(Tab tab){
        return tabRepository.save(tab);
    }


}
